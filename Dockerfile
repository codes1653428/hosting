# Use an official Python runtime as the base image
FROM python:3.9-slim-buster

# Install FFmpeg
RUN apt-get update && \
    apt-get install -y ffmpeg

# Set the working directory in the container
WORKDIR /app

# Copy the requirements.txt file to the container
COPY requirements.txt .

# Install project dependencies using pip
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the project files to the container
COPY alive.py alive.sh keep_alive.py main.py run.sh yt.sh g.mp4 ./

# Set the entry point for the container
CMD ["python", "main.py"]
